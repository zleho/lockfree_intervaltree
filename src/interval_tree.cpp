#include <memory>
#include <iostream>
#include <queue>
#include <atomic>
#include <random>
#include <thread>
#include <algorithm>
#include <chrono>

using Point = unsigned long;
        
class Interval_tree
{
    struct Node;
    static std::shared_ptr<Node> nil;
    friend struct Node;

    struct Node
    {
        std::weak_ptr<Node> parent;
        std::shared_ptr<Node> left;
        std::shared_ptr<Node> right;

        Point lower;
        Point upper;
        Point max;
        unsigned long count;
        bool red;
        std::atomic<bool> flag;
        bool nil;

        Node() : red(false), flag(false), nil(true)
        {
        }

        Node(Point a, Point b) 
            : left(new Node), right(new Node),
              lower(a), upper(b), max(upper), count(1),
              red(true), flag(false), nil(false)
        {
        }

        void update_max()
        {
            max = upper;
            if (left && !left->nil && left->max > max)
            {
                max = left->max;
            }

            if (right && !right->nil && right->max > max)
            {
                max = right->max;
            }
        }
    };

    std::shared_ptr<Node> root, rp, rgp;

    void left_rotate(std::shared_ptr<Node> x)
    {
        auto y = x->right;
        x->right = y->left;
        if (!y->left->nil)
        {
            y->left->parent = x;
        }
        y->parent = x->parent;
        if (x->parent.lock()->nil)
        {
            root = y;
            y->parent = rp;
            rp->left = y;
        }
        else if (x == x->parent.lock()->left)
        {
            x->parent.lock()->left = y;
        }
        else
        {
            x->parent.lock()->right = y;
        }

        y->left = x;
        x->parent = y;
    }

    void right_rotate(std::shared_ptr<Node> x)
    {
        auto y = x->left;
        x->left = y->right;
        if (!y->right->nil)
        {
            y->right->parent = x;
        }
        y->parent = x->parent;
        if (x->parent.lock()->nil)
        {
            root = y;
            y->parent = rp;
            rp->left = y;
        }
        else if (x == x->parent.lock()->right)
        {
            x->parent.lock()->right = y;
        }
        else
        {
            x->parent.lock()->left = y;
        }

        y->right = x;
        x->parent = y;
    }

    void insert_fixup(std::shared_ptr<Node> z, std::vector<std::shared_ptr<Node>>& local_area)
    {
        while (z->parent.lock()->red)
        {
            if (z->parent.lock() == z->parent.lock()->parent.lock()->left)
            {
                auto y = z->parent.lock()->parent.lock()->right;
                if (y->red)
                {
                    z->parent.lock()->red = false;
                    y->red = false;
                    z->parent.lock()->parent.lock()->red = true;

                    auto zp = z->parent.lock();
                    auto zgp = zp->parent.lock();
                    auto zu = zp == zgp->left ? zgp->right : zgp->left;
                    auto zggp = zgp->parent.lock();

                    while (true)
                    {
                        auto expected = false;
                        if (!zggp->flag.compare_exchange_strong(expected, true))
                        {
                            continue;
                        }

                        if (!setup_local_area(zggp, local_area))
                        {
                            zggp->flag = false;
                            continue;
                        }

                        break;
                    }

                    local_area.push_back(zggp);

                    z->flag = zp->flag = zu->flag = false;
                    local_area.erase(std::remove(local_area.begin(), local_area.end(), z), local_area.end());
                    local_area.erase(std::remove(local_area.begin(), local_area.end(), zp), local_area.end());
                    local_area.erase(std::remove(local_area.begin(), local_area.end(), zu), local_area.end());

                    z = zgp;
                }
                else
                {
                    if (z == z->parent.lock()->right)
                    {
                        z = z->parent.lock();
                        left_rotate(z);
                    }

                    z->parent.lock()->red = false;
                    z->parent.lock()->parent.lock()->red = true;
                    right_rotate(z->parent.lock()->parent.lock());
                }
            }
            else
            {
                auto y = z->parent.lock()->parent.lock()->left;
                if (y->red)
                {
                    z->parent.lock()->red = false;
                    y->red = false;
                    z->parent.lock()->parent.lock()->red = true;

                    auto zp = z->parent.lock();
                    auto zgp = zp->parent.lock();
                    auto zu = zp == zgp->left ? zgp->right : zgp->left;
                    auto zggp = zgp->parent.lock();

                    while (true)
                    {
                        auto expected = false;
                        if (!zggp->flag.compare_exchange_strong(expected, true))
                        {
                            continue;
                        }

                        if (!setup_local_area(zggp, local_area))
                        {
                            zggp->flag = false;
                            continue;
                        }

                        break;
                    }

                    local_area.push_back(zggp);

                    z->flag = zp->flag = zu->flag = false;
                    local_area.erase(std::remove(local_area.begin(), local_area.end(), z), local_area.end());
                    local_area.erase(std::remove(local_area.begin(), local_area.end(), zp), local_area.end());
                    local_area.erase(std::remove(local_area.begin(), local_area.end(), zu), local_area.end());
 
                    z = zgp;
                }
                else
                {
                    if (z == z->parent.lock()->left)
                    {
                        z = z->parent.lock();
                        right_rotate(z);
                    }

                    z->parent.lock()->red = false;
                    z->parent.lock()->parent.lock()->red = true;
                    left_rotate(z->parent.lock()->parent.lock());
                }
            }
        }

        for (auto& node : local_area)
        {
            node->flag = false;
        }

        root->red = false;
    }

    bool setup_local_area(std::shared_ptr<Node> zp, std::vector<std::shared_ptr<Node>>& local_area)
    {
        auto zgp = zp->parent.lock();
        auto expected = false;

        if (!zgp->flag.compare_exchange_strong(expected, true))
        {
            return false;
        }

        if (zp->parent.lock() != zgp)
        {
            zgp->flag = false;
            return false;
        }

        auto zu = zp == zgp->left ? zgp->right : zgp->left;
        expected = false;
        
        if (!zu->flag.compare_exchange_strong(expected, true))
        {
            zgp->flag = false;
            return false;
        }

        local_area.push_back(zgp);
        local_area.push_back(zu);
        return true;
    }

    void insert_node(std::shared_ptr<Node> z)
    {
        std::shared_ptr<Node> x, y;
restart:

        auto expected = false;
        while (!root->flag.compare_exchange_strong(expected, true))
            ;

        y = rp;
        x = root;

        while (!x->nil)
        {
            if (z->lower == x->lower && z->upper == x->upper)
            {
                if (!y->nil)
                {
                    y->flag = false;
                }

                x->count += z->count;
                x->flag = false;
                return;
            }

            y = x;
            if (z->lower < x->lower)
            {
                x = x->left;
            }
            else
            {
                x = x->right;
            }

            auto expected = false;
            if (!x->flag.compare_exchange_strong(expected, true))
            {
                y->flag = false;
                goto restart;
            }

            if (!x->nil)
            {
                y->flag = false;
            }
        }

        z->flag = true;
        std::vector<std::shared_ptr<Node>> local_area;
        local_area.push_back(z);
        local_area.push_back(y);

        if (!setup_local_area(y, local_area))
        {
            local_area.clear();
            y->flag = false;
            goto restart;
        }

        z->parent = y;
        
        if (y->nil)
        {
            root = z;
            rp->left = z;
            z->parent = rp;
        }
        else if (z->lower < y->lower)
        {
            y->left = z;
        }
        else
        {
            y->right = z;
        }

        insert_fixup(z, local_area);
    }

    static void print_tree_by_node(std::shared_ptr<Node> x, int level = 0)
    {
        if (!x)
        {
            return;
        }

        for (int i = 0; i < level; ++i)
        {
            std::cout << '\t';
        }

        if (x->nil)
        {
            std::cout << "|-- nil (flag=" << x->flag << ")\n";
        }
        else
        {
            std::cout << "|-- ([" << x->lower << ',' << x->upper << "],count=" << x->count << ",max=" << x->max << ",red=" << x->red << ",flag=" << x->flag << ")\n";
        }

        print_tree_by_node(x->left, level + 1);
        print_tree_by_node(x->right, level + 1);
    }

    void update_node(std::shared_ptr<Node> x)
    {
        if (x && !x->nil)
        {
            x->max = x->upper;
            if (x->left && !x->left->nil)
            {
                update_node(x->left);
                if (x->left->max > x->max)
                {
                    x->max = x->left->max;
                }
            }

            if (x->right && !x->right->nil)
            {
                update_node(x->right);
                if (x->right->max > x->max)
                {
                    x->max = x->right->max;
                }
            }
        }
    }
public:
    void print_tree()
    {
        print_tree_by_node(rgp);
    }

    Interval_tree() 
        : root(new Node), 
          rp(new Node),
          rgp(new Node)
    {
        rgp->left = rp;
        rgp->right = std::make_shared<Node>();
        rgp->right->parent = rgp;
        rp->right = std::make_shared<Node>();
        rp->right->parent = rp;
        rp->left = root;
        rp->parent = rgp;
        root->parent = rp;
    }

    void insert(Point a, Point b)
    {
        auto z = std::make_shared<Node>(a, b);
        insert_node(std::move(z));
    }

    unsigned long count(Point p)
    {
        auto x = root;
        while (x && (p < x->lower || x->upper < p))
        {
            if (x->left && x->left->max >= p)
            {
                x = x->left;
            }
            else
            {
                x = x->right;
            }
        }

        unsigned long count = 0;
        if (x)
        {
            std::queue<std::shared_ptr<Node>> q;
            q.push(x);

            while (!q.empty())
            {
                auto& node = q.front();

                if (node->lower <= p && p <= node->upper)
                {
                    count += node->count;
                }
                
                if (node->left && node->left->lower <= p && p <= node->left->max)
                {
                    q.push(node->left);
                }
                
                if (node->right && node->right->lower <= p && p <= node->right->max)
                {
                    q.push(node->right);
                }

                q.pop();
            }
        }

        return count;
    }

    void update()
    {
        update_node(root);
    }

    bool check_flags_by_node(std::shared_ptr<Node> x)
    {
        if (x->flag)
        {
            return true;
        }

        return (x->left && check_flags_by_node(x->left)) || 
               (x->right && check_flags_by_node(x->right));
    }

    bool check_flags()
    {
        return check_flags_by_node(rgp);
    }
};

void inserter(Interval_tree& it, Point a, Point b, int N)
{
    std::hash<std::thread::id> hasher;

    std::default_random_engine gen(hasher(std::this_thread::get_id()));
    std::uniform_int_distribution<Point> dist(a, b);

    for (int i = 0; i < N; ++i)
    {
        auto x = dist(gen);
        auto y = dist(gen);
        if (x > y)
        {
            std::swap(x, y);
        }
        it.insert(x, y);
    }
}

static const int max_threads = 10;
static const int sample_size = 10;
static const int batch_size = 1000;
static const int run_size = 10;
int main()
{
    std::vector<std::vector<std::vector<double>>> samples;
    
    for (int n = 0; n < run_size; ++n)
    {
        samples.emplace_back();

        int N = (n+1) * batch_size;

        for (int t = 1; t <= max_threads; t *= 2)
        {
            samples.back().emplace_back();

            for (int i = 0; i < sample_size; ++i)
            {
                std::cout << N << ' ' << t << ' ' << i << '\n';
                auto start = std::chrono::steady_clock::now();
                std::vector<std::thread> workers;
                Interval_tree it;
                for (int j = 0; j < t; ++j)
                {
                    workers.emplace_back([&]{ inserter(it, 0, 10000, N / t); });
                }

                for (auto& worker : workers)
                {
                    worker.join();
                }

                samples.back().back().emplace_back(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count());
            }
        }
    }

    std::vector<std::vector<double>> stat;
    for (const auto& run : samples)
    {
        stat.emplace_back();

        for (const auto& thread : run)
        {
            double sum = 0.0;
            for (const auto &sample : thread)
            {
                sum += sample;
            }

            stat.back().emplace_back(sum / sample_size);
        }
    }

    for (int t = 1; t <= max_threads; t*=2)
    {
        std::cout << ',' << t;
    }
    std::cout << '\n';

    for (int n = 0; n < run_size; ++n)
    {
        std::cout << (n+1) * batch_size;
        for (const auto& sample : stat[n])
        {
            std::cout << ',' << sample;
        }
        std::cout << '\n';
    }

    return 0;
}

