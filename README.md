# Parallel lock free interval tree

## Problem statement

Given N closed intervals, create a data structure parallelly, in which point intersection can be calcluated fast!

## Solution

The selected data strucuture is lock-free red-black tree, key is the minimum value of the interval. Every node contains an additional field, `max`, which is the maximum value of the subtree rooted at the node.
The single threaded version of this data structure can be found in _Introduction to algorithms_ by Cormen _et. al_. Lock free extensions are intruduced in several paper, the current work is based on [Lock free red-balck-trees](https://www.cs.umanitoba.ca/~hacamero/Research/RBTreesKim.pdf). We are interested in insert-only datatructure, some ideas can be found [here](https://xuezhaokun.github.io/150-algorithm/).

The idea is to always maintain (lock via CAS) a local area for each insertion: the node to be inserted, its parent, its grandparent, and its uncle (sibling of the parent node).

The `max` field of the nodes requires cannot be handled locally, since it requires more nodes ot be locked than the local area. For this reason it's update is postponed until every node is inserted to the tree.

Since we only interesed in the count of intersecting intervals, if during insert we realize, that the node is already inserted, we don't insert it again, but increase a counter inside the node.

## Results

TODO
